# mail delivery trains

## Problem

You’ve been asked to write a program to control a network of autonomous mail delivery trains.  
Each instance of this problem has:
A network, consisting of a set of nodes, each described by a (string) name.  
A set of edges, each of which links two nodes and has an associated journey time in seconds.
Edges are undirected and any number of trains can travel along any edge in any combination of
orders. An edge is uniquely described by a pair of nodes.  
A set of trains in the network, each of which has a maximum total weight it can carry. All trains start
off empty and each train has a node where it starts off.  
There is a set of packages in the network, each of which has a weight and starts off located at a
node, and each of which has a destination node.  
The problem is solved when all packages are located at their destination nodes.

## How I came to solution

The problem is related to [Vehicle routing problem](https://en.wikipedia.org/wiki/Vehicle_routing_problem),
in particular it is Vehicle Routing Problem with Pickup and Delivery (VRPPD).
This is NP-Hard problem that requires extensive and deep research to find the best solution.

So I decided to develop lightweight solution, when each train picks up single package and carries it to
destination, then picks up next, etc. This solution requires to know the shortest path from
any graph's node to any other node. This is
[all-pairs shortest paths](https://en.wikipedia.org/wiki/Shortest_path_problem#All-pairs_shortest_paths)
problem. To get all shortest paths I decided to use
[Floyd–Warshall algorithm](https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm).
This algorithm has time complexity O(V^3) where V - is number of vertexes in graph. But it needed
to run only once when program starts, after that we can build the shortest paths very quickly.

## Examples

Examples are checked in `/tests` folder.

### Legend

* Circles are nodes in graph (or cities)
* P1, P2, ... - packages with weight and destination
* T1, T2, ... - trains with some capacity

### First case

![Alt text](images/simple.png?raw=true "simple")

This gives these logs:
```
time: 0; train T1; moving from 1 to 0, it takes 30 minutes; want to take P1 package
time: 30; train T1; moving from 0 to 1, it takes 30 minutes; relocating P1 package
time: 60; train T1; moving from 1 to 2, it takes 10 minutes; relocating P1 package
time: 70; train T1; idle
```

### Second case

![Alt text](images/complex.png?raw=true "complex")

This gives these logs:
```
time: 0; train T1; moving from 0 to 2, it takes 8 minutes; relocating P2 package
time: 0; train T2; moving from 2 to 4, it takes 2 minutes; want to take P3 package
time: 0; train T3; moving from 2 to 0, it takes 8 minutes; want to take P1 package
time: 2; train T2; moving from 4 to 2, it takes 2 minutes; relocating P3 package
time: 4; train T2; moving from 2 to 1, it takes 4 minutes; relocating P3 package
time: 8; train T1; moving from 2 to 4, it takes 2 minutes; relocating P2 package
time: 8; train T2; moving from 1 to 3, it takes 1 minutes; relocating P3 package
time: 8; train T3; moving from 0 to 1, it takes 10 minutes; relocating P1 package
time: 9; train T2; idle
time: 10; train T1; idle
time: 18; train T3; moving from 1 to 3, it takes 1 minutes; relocating P1 package
time: 19; train T3; idle
```
