use mail_delivery_trains::{graph, input};

fn test_data() -> input::Data {
    input::Data {
        nodes: (0..=4).map(|num| num.to_string()).collect(),
        edges: vec![
            input::Edge {
                name: "0-1".to_string(),
                from: "0".to_string(),
                to: "1".to_string(),
                time: 1,
            },
            input::Edge {
                name: "1-2".to_string(),
                from: "1".to_string(),
                to: "2".to_string(),
                time: 2,
            },
            input::Edge {
                name: "1-3".to_string(),
                from: "1".to_string(),
                to: "3".to_string(),
                time: 5,
            },
            input::Edge {
                name: "2-3".to_string(),
                from: "2".to_string(),
                to: "3".to_string(),
                time: 2,
            },
            input::Edge {
                name: "2-4".to_string(),
                from: "2".to_string(),
                to: "4".to_string(),
                time: 10,
            },
            input::Edge {
                name: "3-4".to_string(),
                from: "3".to_string(),
                to: "4".to_string(),
                time: 4,
            },
        ],
        packages: vec![],
        trains: vec![],
    }
}

#[test]
fn floyd_warshall() {
    let test_data = test_data();

    let cost = graph::create_adjacency_matrix(&test_data);
    check_cost_before_floyd(&cost);

    let path = graph::create_path_matrix(&cost);
    check_path_before_floyd(&path);

    let graph = graph::floyd_warshall(cost, path).unwrap();

    check_cost_after_floyd(&graph.cost);
    check_path_after_floyd(&graph.path);
    check_paths(&graph);
}

fn check_paths(graph: &graph::Graph) {
    assert_eq!(graph.create_path(0, 1), vec![0, 1]);
    assert_eq!(graph.create_path(0, 2), vec![0, 1, 2]);
    assert_eq!(graph.create_path(0, 3), vec![0, 1, 2, 3]);
    assert_eq!(graph.create_path(0, 4), vec![0, 1, 2, 3, 4]);
    assert_eq!(graph.create_path(4, 1), vec![4, 3, 2, 1]);
    assert_eq!(graph.create_path(4, 2), vec![4, 3, 2]);
    assert_eq!(graph.create_path(2, 4), vec![2, 3, 4]);
    assert_eq!(graph.create_path(1, 4), vec![1, 2, 3, 4]);
    assert_eq!(graph.create_path(0, 0), vec![0]);
    assert_eq!(graph.create_path(3, 0), vec![3, 2, 1, 0]);
    assert_eq!(graph.create_path(3, 1), vec![3, 2, 1]);
}

fn check_cost_before_floyd(cost: &[Vec<graph::Distance>]) {
    use graph::Distance::*;
    let expected = vec![
        vec![Value(0), Value(1), Infinity, Infinity, Infinity],
        vec![Value(1), Value(0), Value(2), Value(5), Infinity],
        vec![Infinity, Value(2), Value(0), Value(2), Value(10)],
        vec![Infinity, Value(5), Value(2), Value(0), Value(4)],
        vec![Infinity, Infinity, Value(10), Value(4), Value(0)],
    ];
    assert_eq!(cost, &expected);
}

fn check_path_before_floyd(path: &[Vec<graph::Path>]) {
    use graph::Path::*;
    let expected = vec![
        vec![Index(0), Index(1), Empty, Empty, Empty],
        vec![Index(0), Index(0), Index(2), Index(3), Empty],
        vec![Empty, Index(1), Index(0), Index(3), Index(4)],
        vec![Empty, Index(1), Index(2), Index(0), Index(4)],
        vec![Empty, Empty, Index(2), Index(3), Index(0)],
    ];
    assert_eq!(path, &expected);
}

fn check_cost_after_floyd(cost: &[Vec<u8>]) {
    let expected = vec![
        vec![0, 1, 3, 5, 9],
        vec![1, 0, 2, 4, 8],
        vec![3, 2, 0, 2, 6],
        vec![5, 4, 2, 0, 4],
        vec![9, 8, 6, 4, 0],
    ];
    assert_eq!(cost, &expected);
}

fn check_path_after_floyd(path: &[Vec<usize>]) {
    let expected = vec![
        vec![0, 1, 1, 1, 1],
        vec![0, 0, 2, 2, 2],
        vec![1, 1, 0, 3, 3],
        vec![2, 2, 2, 0, 4],
        vec![3, 3, 3, 3, 0],
    ];
    assert_eq!(path, &expected);
}
