use itertools::Itertools;

use mail_delivery_trains::input::{Package, Train};
use mail_delivery_trains::simulation::core::Simulation;
use mail_delivery_trains::simulation::entity::MoveType::{Relocating, ToPackage};
use mail_delivery_trains::simulation::event::Event;
use mail_delivery_trains::simulation::event::EventType::{Idle, Moving};
use mail_delivery_trains::{graph, input};

fn test_data() -> input::Data {
    input::Data {
        nodes: (0..=4).map(|num| num.to_string()).collect(),
        edges: vec![
            input::Edge {
                name: "0-1".to_string(),
                from: "0".to_string(),
                to: "1".to_string(),
                time: 10,
            },
            input::Edge {
                name: "0-2".to_string(),
                from: "0".to_string(),
                to: "2".to_string(),
                time: 8,
            },
            input::Edge {
                name: "0-4".to_string(),
                from: "0".to_string(),
                to: "4".to_string(),
                time: 12,
            },
            input::Edge {
                name: "1-2".to_string(),
                from: "1".to_string(),
                to: "2".to_string(),
                time: 4,
            },
            input::Edge {
                name: "1-3".to_string(),
                from: "1".to_string(),
                to: "3".to_string(),
                time: 1,
            },
            input::Edge {
                name: "1-4".to_string(),
                from: "1".to_string(),
                to: "4".to_string(),
                time: 8,
            },
            input::Edge {
                name: "2-4".to_string(),
                from: "2".to_string(),
                to: "4".to_string(),
                time: 2,
            },
        ],
        packages: vec![
            Package {
                name: "P1".to_string(),
                from: "0".to_string(),
                to: "3".to_string(),
                weight: 10,
            },
            Package {
                name: "P2".to_string(),
                from: "0".to_string(),
                to: "4".to_string(),
                weight: 5,
            },
            Package {
                name: "P3".to_string(),
                from: "4".to_string(),
                to: "3".to_string(),
                weight: 5,
            },
        ],
        trains: vec![
            Train {
                name: "T1".to_string(),
                location: "0".to_string(),
                capacity: 5,
            },
            Train {
                name: "T2".to_string(),
                location: "2".to_string(),
                capacity: 6,
            },
            Train {
                name: "T3".to_string(),
                location: "2".to_string(),
                capacity: 12,
            },
        ],
    }
}

#[test]
fn second() {
    let test_data = test_data();

    let cost = graph::create_adjacency_matrix(&test_data);
    let path = graph::create_path_matrix(&cost);

    let graph = graph::floyd_warshall(cost, path).unwrap();

    let result = Simulation::new(graph, test_data).events().collect_vec();

    let expected = vec![
        Event {
            current_time: 0,
            train_name: "T1".to_owned(),
            event_type: Moving {
                from: 0,
                to: 2,
                time_offset: 8,
                package_name: "P2".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 0,
            train_name: "T2".to_owned(),
            event_type: Moving {
                from: 2,
                to: 4,
                time_offset: 2,
                package_name: "P3".to_owned(),
                move_type: ToPackage,
            },
        },
        Event {
            current_time: 0,
            train_name: "T3".to_owned(),
            event_type: Moving {
                from: 2,
                to: 0,
                time_offset: 8,
                package_name: "P1".to_owned(),
                move_type: ToPackage,
            },
        },
        Event {
            current_time: 2,
            train_name: "T2".to_owned(),
            event_type: Moving {
                from: 4,
                to: 2,
                time_offset: 2,
                package_name: "P3".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 4,
            train_name: "T2".to_owned(),
            event_type: Moving {
                from: 2,
                to: 1,
                time_offset: 4,
                package_name: "P3".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 8,
            train_name: "T1".to_owned(),
            event_type: Moving {
                from: 2,
                to: 4,
                time_offset: 2,
                package_name: "P2".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 8,
            train_name: "T2".to_owned(),
            event_type: Moving {
                from: 1,
                to: 3,
                time_offset: 1,
                package_name: "P3".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 8,
            train_name: "T3".to_owned(),
            event_type: Moving {
                from: 0,
                to: 1,
                time_offset: 10,
                package_name: "P1".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 9,
            train_name: "T2".to_owned(),
            event_type: Idle,
        },
        Event {
            current_time: 10,
            train_name: "T1".to_owned(),
            event_type: Idle,
        },
        Event {
            current_time: 18,
            train_name: "T3".to_owned(),
            event_type: Moving {
                from: 1,
                to: 3,
                time_offset: 1,
                package_name: "P1".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 19,
            train_name: "T3".to_owned(),
            event_type: Idle,
        },
    ];
    assert_eq!(result, expected);
}
