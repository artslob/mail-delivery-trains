use itertools::Itertools;

use mail_delivery_trains::input::{Package, Train};
use mail_delivery_trains::simulation::core::Simulation;
use mail_delivery_trains::simulation::entity::MoveType::{Relocating, ToPackage};
use mail_delivery_trains::simulation::event::Event;
use mail_delivery_trains::simulation::event::EventType::{Idle, Moving};
use mail_delivery_trains::{graph, input};

fn test_data() -> input::Data {
    input::Data {
        nodes: (0..=2).map(|num| num.to_string()).collect(),
        edges: vec![
            input::Edge {
                name: "0-1".to_string(),
                from: "0".to_string(),
                to: "1".to_string(),
                time: 30,
            },
            input::Edge {
                name: "1-2".to_string(),
                from: "1".to_string(),
                to: "2".to_string(),
                time: 10,
            },
        ],
        packages: vec![Package {
            name: "K1".to_string(),
            from: "0".to_string(),
            to: "2".to_string(),
            weight: 5,
        }],
        trains: vec![Train {
            name: "Q1".to_string(),
            location: "1".to_string(),
            capacity: 6,
        }],
    }
}

#[test]
fn first() {
    let test_data = test_data();

    let cost = graph::create_adjacency_matrix(&test_data);
    let path = graph::create_path_matrix(&cost);

    let graph = graph::floyd_warshall(cost, path).unwrap();

    let result = Simulation::new(graph, test_data).events().collect_vec();

    let expected = vec![
        Event {
            current_time: 0,
            train_name: "Q1".to_owned(),
            event_type: Moving {
                from: 1,
                to: 0,
                time_offset: 30,
                package_name: "K1".to_owned(),
                move_type: ToPackage,
            },
        },
        Event {
            current_time: 30,
            train_name: "Q1".to_owned(),
            event_type: Moving {
                from: 0,
                to: 1,
                time_offset: 30,
                package_name: "K1".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 60,
            train_name: "Q1".to_owned(),
            event_type: Moving {
                from: 1,
                to: 2,
                time_offset: 10,
                package_name: "K1".to_owned(),
                move_type: Relocating,
            },
        },
        Event {
            current_time: 70,
            train_name: "Q1".to_owned(),
            event_type: Idle,
        },
    ];
    assert_eq!(result, expected);
}
