use super::entity;
use crate::graph;
use crate::input;
use crate::simulation::event::Event;
use itertools::Itertools;

pub struct Simulation {
    graph: graph::Graph,
    packages: Vec<entity::Package>,
    trains: Vec<entity::Train>,
    current_time: Option<u32>,
}

impl Simulation {
    pub fn new(graph: graph::Graph, input: input::Data) -> Self {
        let mut packages = input
            .packages
            .iter()
            .map(|package| entity::Package::from_input(package, &input))
            .collect_vec();

        let trains = input
            .trains
            .iter()
            .map(|train| entity::Train::from_input(train, &input, &graph, &mut packages))
            .collect_vec();

        Self {
            graph,
            packages,
            trains,
            current_time: Some(0),
        }
    }

    fn run(&mut self) -> Option<Vec<Event>> {
        let current_time = self.current_time?;

        let events = self
            .trains
            .iter_mut()
            .filter(|train| train.next_event_time == Some(current_time))
            .map(|train| train.update(current_time, &self.graph, &mut self.packages))
            .collect_vec();

        self.current_time = self
            .trains
            .iter()
            .flat_map(|train| train.next_event_time)
            .min();

        Some(events)
    }

    pub fn events(&mut self) -> impl Iterator<Item = Event> + '_ {
        std::iter::repeat_with(|| self.run()).while_some().flatten()
    }
}
