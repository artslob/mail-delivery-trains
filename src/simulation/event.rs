use crate::simulation::entity::MoveType;

#[derive(Debug, Eq, PartialEq)]
pub struct Event {
    pub current_time: u32,
    pub train_name: String,
    pub event_type: EventType,
}

impl Event {
    pub fn print(&self) {
        let current_time = self.current_time;
        match &self.event_type {
            EventType::Idle => {
                println!("time: {}; train {}; idle", current_time, self.train_name);
            }
            EventType::Moving {
                from,
                to,
                time_offset,
                package_name,
                move_type,
            } => match move_type {
                MoveType::ToPackage => println!(
                    "time: {}; train {}; moving from {} to {}, it takes {} minutes; want to take {} package",
                    current_time, self.train_name, from, to, time_offset, package_name
                ),
                MoveType::Relocating => println!(
                    "time: {}; train {}; moving from {} to {}, it takes {} minutes; relocating {} package",
                    current_time, self.train_name, from, to, time_offset, package_name
                ),
            },
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum EventType {
    Idle,
    Moving {
        from: usize,
        to: usize,
        time_offset: u8,
        package_name: String,
        move_type: MoveType,
    },
}
