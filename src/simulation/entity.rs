use super::event::{Event, EventType};
use crate::graph;
use crate::input;
use crate::utils;

#[derive(Debug)]
pub struct Train {
    pub name: String,
    pub capacity: u8,
    pub location: usize,
    pub state: TrainState,
    pub next_event_time: Option<u32>,
}

// TODO check case when package already at destination

impl Train {
    pub fn from_input(
        train: &input::Train,
        input: &input::Data,
        graph: &graph::Graph,
        packages: &mut Vec<Package>,
    ) -> Self {
        let location = utils::find_index_by_name(input, &train.location);
        let state = Self::next_state_after_delivery(packages, location, train.capacity, graph);
        Self {
            name: train.name.clone(),
            capacity: train.capacity,
            location,
            state,
            next_event_time: Some(0),
        }
    }

    pub fn update(
        &mut self,
        current_time: u32,
        graph: &graph::Graph,
        packages: &mut Vec<Package>,
    ) -> Event {
        match &self.state {
            TrainState::Moving { package, move_type } => {
                let from = self.location;
                let final_destination = match move_type {
                    MoveType::ToPackage => package.from,
                    MoveType::Relocating => package.to,
                };
                let to = graph.next_node(from, final_destination);
                let time_offset = graph.next_time(from, to);
                self.next_event_time = Some(current_time + time_offset as u32);
                self.location = to;
                let event = Event {
                    current_time,
                    train_name: self.name.clone(),
                    event_type: EventType::Moving {
                        from,
                        to,
                        time_offset,
                        package_name: package.name.clone(),
                        move_type: *move_type,
                    },
                };
                if self.location == final_destination {
                    self.state = match move_type {
                        MoveType::ToPackage => TrainState::Moving {
                            package: package.clone(),
                            move_type: MoveType::Relocating,
                        },
                        MoveType::Relocating => Self::next_state_after_delivery(
                            packages,
                            self.location,
                            self.capacity,
                            graph,
                        ),
                    }
                }
                event
            }
            TrainState::Idle => {
                self.next_event_time = None;
                Event {
                    current_time,
                    train_name: self.name.clone(),
                    event_type: EventType::Idle,
                }
            }
        }
    }

    fn next_state_after_delivery(
        packages: &mut Vec<Package>,
        location: usize,
        capacity: u8,
        graph: &graph::Graph,
    ) -> TrainState {
        let closest_package_index = packages
            .iter()
            .enumerate()
            .filter(|(_, package)| package.weight <= capacity)
            .min_by_key(|(_, package)| graph.next_time(location, package.from))
            .map(|(index, _)| index);
        match closest_package_index {
            None => TrainState::Idle,
            Some(index) => {
                let package = packages.remove(index);
                let move_type = if location == package.from {
                    MoveType::Relocating
                } else {
                    MoveType::ToPackage
                };
                TrainState::Moving { package, move_type }
            }
        }
    }
}

#[derive(Debug)]
pub enum TrainState {
    Moving {
        package: Package,
        move_type: MoveType,
    },
    Idle,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum MoveType {
    ToPackage,
    Relocating,
}

#[derive(Debug, Clone)]
pub struct Package {
    pub name: String,
    pub from: usize,
    pub to: usize,
    pub weight: u8,
}

impl Package {
    pub fn from_input(package: &input::Package, input: &input::Data) -> Self {
        Self {
            name: package.name.clone(),
            from: utils::find_index_by_name(input, &package.from),
            to: utils::find_index_by_name(input, &package.to),
            weight: package.weight,
        }
    }
}
