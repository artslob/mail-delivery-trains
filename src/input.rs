use anyhow::anyhow;

pub fn validate_data(input: &Data) -> anyhow::Result<()> {
    let all_packages_can_be_placed_into_some_train = input.packages.iter().all(|package| {
        input
            .trains
            .iter()
            .any(|train| train.capacity >= package.weight)
    });
    if !all_packages_can_be_placed_into_some_train {
        return Err(anyhow!("some package cannot be moved by trains"));
    }

    Ok(())
}

#[derive(Debug, serde::Deserialize)]
pub struct Data {
    pub nodes: Vec<String>,
    pub edges: Vec<Edge>,
    pub packages: Vec<Package>,
    pub trains: Vec<Train>,
}

#[derive(Debug, serde::Deserialize)]
pub struct Edge {
    pub name: String,
    pub from: String,
    pub to: String,
    pub time: u8,
}

#[derive(Debug, serde::Deserialize)]
pub struct Package {
    pub name: String,
    pub from: String,
    pub to: String,
    pub weight: u8,
}

#[derive(Debug, serde::Deserialize)]
pub struct Train {
    pub name: String,
    pub location: String,
    pub capacity: u8,
}
