use crate::{input, utils};
use anyhow::anyhow;

#[derive(Debug)]
pub struct Graph {
    pub cost: Vec<Vec<u8>>,
    pub path: Vec<Vec<usize>>,
}

impl Graph {
    pub fn create_path(&self, mut from: usize, to: usize) -> Vec<usize> {
        let mut result = vec![from];
        while from != to {
            from = self.path[from][to];
            result.push(from);
        }
        result
    }

    pub fn next_time(&self, from: usize, to: usize) -> u8 {
        self.cost[from][to]
    }

    pub fn next_node(&self, from: usize, to: usize) -> usize {
        self.path[from][to]
    }
}

pub fn floyd_warshall(
    mut cost: Vec<Vec<Distance>>,
    mut path: Vec<Vec<Path>>,
) -> anyhow::Result<Graph> {
    let range = 0..cost.len();
    for k in range.clone() {
        for i in range.clone() {
            for j in range.clone() {
                if let (Distance::Value(ik), Distance::Value(kj)) = (cost[i][k], cost[k][j]) {
                    let is_less = match cost[i][j] {
                        Distance::Infinity => true,
                        Distance::Value(ij) => ik + kj < ij,
                    };
                    if is_less {
                        cost[i][j] = Distance::Value(ik + kj);
                        path[i][j] = path[i][k];
                    }
                }
            }
        }
    }
    Ok(Graph {
        cost: post_process_cost(cost)?,
        path: post_process_path(path)?,
    })
}

fn post_process_cost(cost: Vec<Vec<Distance>>) -> anyhow::Result<Vec<Vec<u8>>> {
    cost.into_iter()
        .map(|distances| {
            distances
                .into_iter()
                .map(|distance| match distance {
                    Distance::Value(value) => Ok(value),
                    Distance::Infinity => Err(anyhow!("cost have infinity value")),
                })
                .collect()
        })
        .collect()
}

fn post_process_path(path: Vec<Vec<Path>>) -> anyhow::Result<Vec<Vec<usize>>> {
    path.into_iter()
        .map(|distances| {
            distances
                .into_iter()
                .map(|path| match path {
                    Path::Index(value) => Ok(value),
                    Path::Empty => Err(anyhow!("path have empty value")),
                })
                .collect()
        })
        .collect()
}

pub fn create_path_matrix(cost: &[Vec<Distance>]) -> Vec<Vec<Path>> {
    cost.iter()
        .enumerate()
        .map(|(i, vec)| {
            vec.iter()
                .enumerate()
                .map(|(j, distance)| {
                    if i == j {
                        Path::Index(0)
                    } else if matches!(distance, Distance::Value(_)) {
                        Path::Index(j)
                    } else {
                        Path::Empty
                    }
                })
                .collect()
        })
        .collect()
}

pub fn create_adjacency_matrix(input: &input::Data) -> Vec<Vec<Distance>> {
    let mut result: Vec<Vec<Distance>> = (0..input.nodes.len())
        .map(|i| {
            (0..input.nodes.len())
                .map(|j| {
                    if i == j {
                        Distance::Value(0)
                    } else {
                        Distance::Infinity
                    }
                })
                .collect()
        })
        .collect();
    for edge in &input.edges {
        let i = utils::find_index_by_name(input, &edge.from);
        let j = utils::find_index_by_name(input, &edge.to);
        result[i][j] = Distance::Value(edge.time);
        result[j][i] = Distance::Value(edge.time);
    }
    result
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Distance {
    Value(u8),
    Infinity,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Path {
    Empty,
    Index(usize),
}
