use crate::input;

pub fn find_index_by_name(input: &input::Data, name: &str) -> usize {
    // TODO get nodes instead of whole input
    input
        .nodes
        .iter()
        .enumerate()
        .flat_map(|(index, node_name)| (node_name == name).then(|| index))
        .next()
        .unwrap()
}
