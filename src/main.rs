use mail_delivery_trains::{graph, input, simulation};

fn main() -> anyhow::Result<()> {
    let file = std::fs::File::open("input.yaml")?;
    let input: input::Data = serde_yaml::from_reader(file)?;
    input::validate_data(&input)?;

    let cost = graph::create_adjacency_matrix(&input);
    let path = graph::create_path_matrix(&cost);
    let graph = graph::floyd_warshall(cost, path)?;

    simulation::core::Simulation::new(graph, input)
        .events()
        .for_each(|event| event.print());

    Ok(())
}
